# dq-pub-files

Public files. (similar to https://github.com/ducquoc/legacy_m2_repo , but more general, like free books, utilities)

Also provides a sparse-checkout structure for limited access practice/trial.

Example: 

```
./sparse-checkout-ductest/EXPORTED/war.ear
```

(TBC)

